
def checkInput(gifts):
    
    output = [] 
    
    # 0 - check if anna is jelly
    if(gifts[1] >  gifts[0] and gifts[2] > gifts[0]):
        output.append("Anna")
    # 1 - check if laura is jelly
    if(gifts[0] >  gifts[1]):
        output.append("Laura")

    # 2 - check if oscar is jelly
    if( (gifts[0] > gifts[2] or gifts[1] > gifts[2]) 
    or (gifts[0] > gifts[2] and gifts[1] > gifts[2]) ):
        output.append("Oscar")

    # 3 - return the list
    if(len(output) == 0):
        output.append("NONE")
        return output
    else:
        return output


# get the input from the user
textInput = input()

# filter the input into a list
giftNumbers = list(map(int, textInput.split()))

#call the function
out = checkInput(giftNumbers)

# print the output from the function
for i in out:
    print(i)

# ------------------------------------------------------
# made by
# Alexander Ziegler
# S181100
