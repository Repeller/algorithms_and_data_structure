
def findMax(numList, count):
    max = 0

    for i in range(0, (count)):
        if(numList[i] > numList[max]):
            max = i
        
    return max

# get the count input from the user
countInput = input() # 15

# get the list of numbers from the user
listInput = input() # 1 3 7 15 17 11 2 3 6 8 7 5 9 5 23

# filter the input into a list
listNumbers = list(map(int, listInput.split()))

#call the function
out = findMax(listNumbers, int(countInput))

# print the output from the function
print(out)
    

# ------------------------------------------------------
# made by
# Alexander Ziegler
# S181100
