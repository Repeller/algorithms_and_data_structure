import math

def peak3(numList, count, offset):
    m = int( math.floor( ( count + offset) / 2 ) ) 

    # case 0
    #if()

    # case 1
    if(numList[m-1] >= numList[m]):
        return m
    # case 2
    elif(numList[m-2] > numList[m-1]):
        return peak3(numList, count, m-1)
    # case 3
    elif(numList[m-1] < numList[m]):
        return peak3(numList, count, m+1)


# get the count input from the user
countInput = input() # 15

# get the list of numbers from the user
listInput = input() # 1 3 7 15 17 11 2 3 6 8 7 5 9 5 23

# filter the input into a list
listNumbers = list(map(int, listInput.split()))

#call the function
out = peak3(listNumbers, int(countInput), 0)

# print the output from the function
print(out)
    

# ------------------------------------------------------
# made by
# Alexander Ziegler
# S181100
