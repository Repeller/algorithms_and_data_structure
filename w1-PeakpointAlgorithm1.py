
def peak1(numList, count):
    if(numList[0] >= numList[1]):
        return 0
    
    for x in range(1, (count - 2)):
        if(numList[x-1] <= numList[x] >= numList[x+1]):
            return x
        
    if (numList[count-2] <= numList[count-1]):
        return (count - 1)


# get the count input from the user
countInput = input() # 15

# get the list of numbers from the user
listInput = input() # 1 3 7 15 17 11 2 3 6 8 7 5 9 5 23

# filter the input into a list
listNumbers = list(map(int, listInput.split()))

#call the function
out = peak1(listNumbers, int(countInput))

# print the output from the function
print(out)
    

# ------------------------------------------------------
# made by
# Alexander Ziegler
# S181100
