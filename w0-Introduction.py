

# this function adds all the numbers that the user have entered
def addNumbers(listOfNumbers):
    # list is bigger than 0
    if(len(listOfNumbers) > 0):
        output = 0
        for item in listOfNumbers:
            if(isinstance(int(item), int)):
                output += int(item)
        
        return output

    else: # when the list have a len lower than 1
        return 0


# the code that gets run goes here

# get the input from the user
textInput = input()

# filter the input into a list
input_numbers = list(map(int, textInput.split()))

#call the function
out = addNumbers(input_numbers)

# print the output from the function
print(str(out))

# made by
# Alexander Ziegler
# S181100